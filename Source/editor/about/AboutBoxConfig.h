//
// This file is part of LibreArp
//
// LibreArp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LibreArp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see https://librearp.gitlab.io/license/.
//

#pragma once

#define WEBSITE_URL "https://librearp.gitlab.io/"
#define LICENSE_URL "https://librearp.gitlab.io/license/"
#define SOURCE_URL "https://gitlab.com/LibreArp/LibreArp"
#define JUCE_WEBSITE_URL "https://juce.com/"

#define LICENSE_NAME "GNU General Public License v3"

#define LICENSE_NOTICE L"LibreArp - A libre VST arpeggio generator\n" \
"Copyright © 2018 The LibreArp contributors\n" \
"\n" \
"This program is free software: you can redistribute it and/or modify " \
"it under the terms of the GNU General Public License as published by " \
"the Free Software Foundation, either version 3 of the License, or " \
"(at your option) any later version.\n" \
"\n" \
"This program is distributed in the hope that it will be useful, " \
"but WITHOUT ANY WARRANTY; without even the implied warranty of " \
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the " \
"GNU General Public License for more details."
